/// <reference types="Cypress"/>
describe('projecttest', () => {
    it('CV content', () => {
        cy.visit('http://127.0.0.1:5500/CV.html')
        cy.get('.card-title').should('contain.text', 'ATTAPOL SUWANNO')
    })
    it('CV content  attribult', () => {
        cy.visit('http://127.0.0.1:5500/CV.html')
        cy.get(':nth-child(2) > .col-12 > :nth-child(5)').should('contain.text', 'Weight/Higth : 56 kg./174 cm.')
    })
    it('content click to Special', () => {
        cy.visit('http://127.0.0.1:5500/CV.html')
        cy.get('#speciality').click()
        cy.get(':nth-child(7) > .col-12 > h1').should('contain.text', 'Speciality')
    })
    it('content click to preferent', () => {
        cy.visit('http://127.0.0.1:5500/CV.html')
        cy.get('#preferent').click()
        cy.get('.col-12 > [style="background-color: #f3efed;"] > .col-4 > h1').should('contain.text', 'Preferent')
    })
    it('content click to history', () => {
        cy.visit('http://127.0.0.1:5500/CV.html')
        cy.get('#table5 > .justify-content-center > .col-4 > h1').should('contain.text', 'History Images')
    })
    it('content click to Faverite', () => {
        cy.visit('http://127.0.0.1:5500/CV.html')
        cy.get('.media-body > .shadow-none').should('contain.text', 'Naruto')
    })
    it('Check link Anime', () => {
        cy.visit('http://127.0.0.1:5500/CV.html')
        cy.get('#btnGroupDrop1').click()
        cy.get('.dropdown-menu > [href="anime.html"]').click()
        cy.get('.table > :nth-child(2) > :nth-child(2)').should('contain.text', 'Naruto')
    })
    it('Check Anime serech Naruto', () => {
        cy.visit('http://127.0.0.1:5500/Anime.html')
        cy.get('#queryId').click()
            .type('Naruto')
        cy.get('.btn').click()
        cy.get(':nth-child(2) > :nth-child(2) > th').should('contain.text', '20')
    })
    it('Check Anime serech Pandora Voxx Complete', () => {
        cy.visit('http://127.0.0.1:5500/Anime.html')
        cy.get('#queryId').click()
            .type('Pandora Voxx Complete')
        cy.get('.btn').click()
        cy.get(':nth-child(2) > :nth-child(2) > :nth-child(2)').should('contain.text', 'Pandora Voxx Complete')
    })
    it('Check Anime link to  Grede Report', () => {
        cy.visit('http://127.0.0.1:5500/Anime.html')
        cy.get(':nth-child(2) > .nav-link').click()
        cy.get('h1').should('contain.text', 'Grade Report')
        cy.get(':nth-child(2) > .nav-link').click()
    })
    it('Anime Check back to CV ', () => {
        cy.visit('http://127.0.0.1:5500/Anime.html')
        cy.get(':nth-child(1) > .nav-link').click()
        cy.get('.card-title').should('contain.text', 'ATTAPOL SUWANNO')
    })
    it('Check link to Grade report ', () => {
        cy.visit('http://127.0.0.1:5500/CV.html')
        cy.get('#btnGroupDrop1').click()
        cy.get('[href="Grade.html"]').click()
        cy.get(':nth-child(17) > .col-6 > .table > tbody > :nth-child(2) > :nth-child(4)').should('contain.text', '4.00')
    })
    it('Check link to Grade report check grade', () => {
        cy.visit('http://127.0.0.1:5500/CV.html')
        cy.get('#btnGroupDrop1').click()
        cy.get('[href="Grade.html"]').click()
        cy.get(':nth-child(17) > .col-6 > .table > tbody > :nth-child(2) > :nth-child(4)').should('contain.text', '4.00')
    })
    it('Check link to Grade report check subject id', () => {
        cy.visit('http://127.0.0.1:5500/Grade.html')
        cy.get(':nth-child(7) > .col-6 > .table > tbody > :nth-child(3) > :nth-child(2)').should('contain.text', '20111')
    })
    it('Check link to Grade report check  subject name ', () => {
        cy.visit('http://127.0.0.1:5500/Grade.html')
        cy.get(':nth-child(7) > .col-6 > .table > tbody > :nth-child(7) > :nth-child(3)').should('contain.text', 'LEARNING THROUGH ACTIVITIES 1')
    })
    it('Grade report back to CV', () => {
        cy.visit('http://127.0.0.1:5500/Grade.html')
        cy.get(':nth-child(1) > .nav-link').click()
        cy.get('.card-title').should('contain.text', 'ATTAPOL SUWANNO')
    })
})