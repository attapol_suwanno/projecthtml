async function loadAllStudentAsync() {
    let response = await fetch('https://api.jikan.moe/v3/search/anime?q=xxx')
    let data = await response.json(); 
    return data
}

function createResultTable(data) {
    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')


    //create the header
    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    
    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)


    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'ID'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Title'
    tableHeadNode.appendChild(tableHeaderNode)


    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Airing'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Synopsis'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Type'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Episodes'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Score'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Members'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'Image'
    tableHeadNode.appendChild(tableHeaderNode)


    console.log(data)

  

    data.then((json) => {
        for (let i = 0; i < 1; i++) {
            var currentData = json["results"][i]
        
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFrirstColumnNode = document.createElement('th')
            dataFrirstColumnNode.setAttribute('scope', 'row')
            dataFrirstColumnNode.innerHTML = currentData['mal_id']
            dataRow.appendChild(dataFrirstColumnNode)


            var columNode = null;

            columNode = document.createElement('td')
            columNode.innerText = currentData['title']
            dataRow.appendChild(columNode)

            columNode = document.createElement('td')
            columNode.innerText = currentData['airing']
            dataRow.appendChild(columNode)

            columNode = document.createElement('td')
            columNode.innerText = currentData['synopsis']
            dataRow.appendChild(columNode)

            columNode = document.createElement('td')
            columNode.innerText = currentData['type']
            dataRow.appendChild(columNode)

            columNode = document.createElement('td')
            columNode.innerText = currentData['episodes']
            dataRow.appendChild(columNode)
    

            columNode = document.createElement('td')
            columNode.innerText = currentData['score']
            dataRow.appendChild(columNode)


            columNode = document.createElement('td')
            columNode.innerText = currentData['members']
            dataRow.appendChild(columNode)


  
            columNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['image_url'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)
    
    
        }
    })
}



async function loadOneStudent() {
    let name = document.getElementById('queryId').value
    if (name != '' && name != null) {
        let response = await fetch('https://api.jikan.moe/v3/search/anime?q='+name)
        let data = await response.json();
        return data
    }
}





async function loadfaverite() {
  
        let response = await fetch('https://api.jikan.moe/v3/search/anime?q=naruto')
        let data2 = await response.json();
        return data2
    
}

